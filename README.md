# Test Challenge Backend

This is a technical test made on PHP

## Challenge

Write a program that prints all the numbers from 1 to 100. However, for
multiples of 3, instead of the number, print "Falabella". For multiples of 5 print
"IT". For numbers which are multiples of both 3 and 5, print "Integraciones".
But here's the catch: you can use only one `if`. No multiple branches, ternary
operators or `else`.

## Requirements

* 1 if
* You can't use `else`, `else if` or ternary
* Unit tests
* Feel free to apply your SOLID knowledge
* You can write the challenge in any language you want. Here at Falabella we are
big fans of PHP, Kotlin and TypeScript
* One git branch for the solution
* One git branch for the documentation

## Steps to test

1. Clone repo and go to branch **dev**

`git clone git@gitlab.com:julioblancorangel/test-challenge-backend.git`

2. Install require packages (In this case only to run unit tests)

`composer install`

3. Check the result of the test on web browser or run on console

`php index.php`

4. Run unit tests

`./vendor/bin/phpunit`

